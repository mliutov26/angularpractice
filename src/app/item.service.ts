export class ItemService {
    items = [
        { name: '#FFFFE0' },
        { name: '#FFFACD' },
        { name: '#FAFAD2' }
    ];
    addItem(name: string) {
        this.items.push({ name });
    }
}