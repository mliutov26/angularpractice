import { Directive, ElementRef, OnInit, Input, Renderer2, HostBinding, HostListener } from '@angular/core';

@Directive({
    selector: '[appBackground]'
})
export class BackgroundDirective implements OnInit {
    @Input() color = '#ff0000';
    @HostBinding('style.backgroundColor') background: string;
    @HostBinding('style.cursor') cursor: string;

    constructor(private element: ElementRef, private renderer: Renderer2) {}

    ngOnInit() {
        this.background = this.color;
        this.cursor = 'pointer';
    }
}
