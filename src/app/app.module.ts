import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Page3Component } from './page3/page3.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ItemComponent } from './item/item.component';
import { ItemAddComponent } from './item-add/item-add.component';

import { ItemService } from './item.service';

import {BackgroundDirective} from './directives/background.directive';

@NgModule({
  declarations: [
      AppComponent,
      Page1Component,
      Page2Component,
      Page3Component,
      HeaderComponent,
      FooterComponent,
      ItemComponent,
      ItemAddComponent,
      BackgroundDirective
  ],
  imports: [
      BrowserModule,
      AppRoutingModule,
      FormsModule
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule { }
