import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';

@Component({
    selector: 'app-page1',
    templateUrl: './page1.component.html',
    styleUrls: ['./page1.component.scss']
})
export class Page1Component implements OnInit {

    items = [];

    constructor(private service: ItemService) {}

    ngOnInit() {
        this.items = this.service.items;
    }

}
