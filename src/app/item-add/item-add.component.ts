import {Component} from '@angular/core';
import {ItemService} from '../item.service';

@Component({
    selector: 'app-item-add',
    templateUrl: './item-add.component.html',
    styleUrls: ['./item-add.component.scss']
})
export class ItemAddComponent {
    itemName = '';

    constructor(private service: ItemService) {}

    addItem() {
        if (this.itemName) {
          this.service.addItem(this.itemName);
        }
        this.itemName = '';
    }
}
